/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

document.addEventListener("deviceready", onDeviceReady, false);

var enabledPermissionsCount = 0;
var permissions;
function onDeviceReady() {
	console.log("device ready");
	permissions = cordova.plugins.permissions;
	permissions.checkPermission(permissions.ACCESS_FINE_LOCATION, function (status) {
		if (status.hasPermission) {
			enabledPermissionsCount++;
			permissionGranted('LOCATION');
		} else {
			requestLocationPermission();
		}
	});
	permissions.checkPermission(permissions.INTERNET, function (status) {
		if (status.hasPermission) {
			enabledPermissionsCount++;
			permissionGranted('INTERNET');
		} else {
			requestInternetPermission();
		}
	});
}

function requestLocationPermission() {
	console.log("req location");
	var list = [
		permissions.ACCESS_COARSE_LOCATION,
		permissions.ACCESS_FINE_LOCATION,
		permissions.ACCESS_LOCATION_EXTRA_COMMANDS
	];

	permissions.requestPermissions(list, function (status) {
		if (status.hasPermission) {
			enabledPermissionsCount++;
			location.reload();
			permissionGranted('LOCATION');
		} else {
			requestLocationPermission();
		}
	});
}

function requestInternetPermission() {
	console.log("req internet");
	var list = [
		permissions.INTERNET,
		permissions.ACCESS_NETWORK_STATE
	];

	permissions.requestPermissions(list, function (status) {
		if (status.hasPermission) {
			enabledPermissionsCount++;
			permissionGranted('INTERNET');
		} else {
			requestInternetPermission();
		}
	});
}

function permissionGranted(string) {
	if (enabledPermissionsCount === 2) {
		console.log(string + ' GRANTED');
		window.location = './map.html';
	}
}