var map, mapCenter = {lat: 43.262122, lng: 26.922293}, currentPositionMarker, codeMarker = [];

function initializeMap() {
	var codePositions = [
		{lat: 43.262122, lng: 26.922293},
		{lat: 43.262013, lng: 26.922485},
		{lat: 43.262097, lng: 26.922365},
		{lat: 43.262225, lng: 26.922315},
		{lat: 43.262172, lng: 26.922262},
		{lat: 43.262052, lng: 26.922150},
		{lat: 43.262247, lng: 26.921917},
		{lat: 43.262227, lng: 26.921885},
		{lat: 43.262235, lng: 26.923008},
		{lat: 43.262497, lng: 26.922202}
	];

	map = new google.maps.Map(document.getElementById('map_canvas'), {
		zoom: 15,
		center: mapCenter,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	var codeIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABHNCSVQICAgIfAhkiAAAAQZJREFUOI2l1b1VwzAQwPH/iZYXGIXUbhjFTACjZINkFHix+jAJWKHmKOLkJZZ1Jxu10v0s34cNwDGy6fd8pUjLwpUiberQsyHHyEaV18sJ4WXVsJuLomyvDek7vgUebk7OwDP0tD6DCG/ZaWVbk5YpVKEPQivGU82bl9A74fm+4SDOK03iHnoKcwLGeA2awR4+KC46CZv4aJXQIlyDW6gJe3gQ1iUUIFiwKo+lvV94smIXp2KILvb5v4pn4VkqjDFdX1rudnNy/N0BGVe/dkLNkS61VA1e/Ah5ferhsgStwSV16BLUxJWPcF3puSjAqmGXdUsYijj8CA8/0Z4m7+Zpz/u59f4AO3vPRW1stxwAAAAASUVORK5CYII=';

	var currentFound = null;
	if(localStorage.getItem("found") != null){
		currentFound = localStorage.getItem("found").split(",");
	}
	for (var i = 0; i < codePositions.length; i++) {
		if(Array.isArray(currentFound) && currentFound[i] !== ""){
			codeIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABHNCSVQICAgIfAhkiAAAAQdJREFUOI2l1LFxwzAMQNEPTuBR4gGUS8U1whSqo2ySnoU5h6rcaYGskgEipvDpLMkkCCtoAbwCBCgAz2P/KZJf5ywfk4+JA9GNfXDCZc68TT4muaK8LwVL4gi6NpwIYV3khEs39mHfbEUBHAxuzgz7YiteQoEfgSBKgTqWKpp5+fLxWxqFRbyFAoihYYNb0Du4hcN1/i20CGt4IYpoFTbiVVSFW7hkzjUUwGkwkk+11C88qa21hGXO2p7/9/Gq+N0olD09Lyu3ASrnbzmQzetbL7R10sWVsuDaJ6TuaQuXI6gFd0dRgMnHVHpQgeB2CTOq4RnSbcYwCIRH0HV0Yx8EQoY0+Zj+APIo4qHaCZAVAAAAAElFTkSuQmCC';
		}
		codeMarker.push(new google.maps.Marker({
			position: codePositions[i],
			map: map,
			icon: codeIcon
		}));
		codeIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABHNCSVQICAgIfAhkiAAAAQZJREFUOI2l1b1VwzAQwPH/iZYXGIXUbhjFTACjZINkFHix+jAJWKHmKOLkJZZ1Jxu10v0s34cNwDGy6fd8pUjLwpUiberQsyHHyEaV18sJ4WXVsJuLomyvDek7vgUebk7OwDP0tD6DCG/ZaWVbk5YpVKEPQivGU82bl9A74fm+4SDOK03iHnoKcwLGeA2awR4+KC46CZv4aJXQIlyDW6gJe3gQ1iUUIFiwKo+lvV94smIXp2KILvb5v4pn4VkqjDFdX1rudnNy/N0BGVe/dkLNkS61VA1e/Ah5ferhsgStwSV16BLUxJWPcF3puSjAqmGXdUsYijj8CA8/0Z4m7+Zpz/u59f4AO3vPRW1stxwAAAAASUVORK5CYII=';
	}

	if (navigator.geolocation) {

		console.log("dsa");
		navigator.geolocation.getCurrentPosition(displayAndWatch, locationError);
	} else {
		$("body").html("HTML5 Not Supported");
	}
}

function locationError(error) {
	alert('Позицията ви не може да бъде намерена..');
}

function displayAndWatch(position) {

	console.log("two");
	// set current position
	setCurrentPosition(position);
	// watch position
	watchCurrentPosition();
}

function setMarkerPosition(marker, position) {
	marker.setPosition(
			new google.maps.LatLng(
					position.coords.latitude,
					position.coords.longitude)
			);
}

function setCurrentPosition(pos) {
	currentPositionMarker = new google.maps.Marker({
		map: map,
		position: new google.maps.LatLng(
				pos.coords.latitude,
				pos.coords.longitude
				),
		title: "Current Position",
		icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAABCCAYAAADjVADoAAAABHNCSVQICAgIfAhkiAAACdtJREFUeJztW21sFMcZfmb2jvMHGB804cuAC2oLsTGglAYQPRu7xpA6+WNBQtomQio/WhKrEYrUSoGSlj+NQklQqhTSH01BLViqqqQEMDEJJNg4WDTgD0JK+aixgBSwzwcx9t3uTH/Ye56bm9nb+7Kqikca7Xl3duZ9n33nnfd9dw08wAM8gALkf3RePiZSCBhrIojmtwiu+Z1VjBURJMHRBk9wzBo82Z5AABlp6Ktd80sPJVtUnUzGf+0/ePgV4dSYWMVYEEHkxsCJzhiHr4EinoCsEpJtImQSKADCOdcuyZFrNhFMvpwlOUGzNG6cFYzMRQEYJufaeUeuGUJ/1VhZETgb44lHCoA2LCnxf3vStEC+Qct9hlFrEDJLNYDFefcgY/v6hsIfz2/66ASGrYILRyiOGRM8U2MR6Te9VFW10J/rfcFL6Y+SHZADQdNie7sH77256NjJK8OnYhqQITIyQYSsPAFAzlcHiqfn5P/BICSQgTkQYWxvZ/+XmwOffBZEPCFpk5EuEXEWAIDcrl29xUfpy2mOHQcOBAfMyEtTD33wJ4wulYxYRzpEyEuBNi4v8y+bXHSUECxMY9yEiDC2d9LBIz9GrO9Ii4xUibDvi3r1LyorF0/Lz9njhgTGeWjIss4zjlDIMs8DQIHheYQSFOR6PEvdCMAYe6+19/rGmpb2PmSAjFTiiLjl8O5jpf5EJDDOQ8FIuOFC6F5jTcunrdJYNjgAtARWrJo1Pnf1RI93rW48SumT35k0nQDta4V7bUIIkiQjWYuI2xYB0NATj7c6kRAyIw3bP+/a+taV63cFoRPO/btFJTOemlH0hs8wlun6jCyTjRgmIWXLSIYIlWM0btfWvOajxibVDYzzUNPN23V1bW1dknCJBIzxPx1VFeuK8/Ne13XuHQo/Nbux6V3Ek+GaiGQjy5go8VxFxSIdCRHGuvZd6/5eXVtbpyCgBcAUWkRq5kgfuzEAbMGx4weO3rhVzTgPqeby+8btblhS4kcsgUASDzqVEDtKxuwJubtVHRjnob/0XNuw6WzXNYySYLpsMiEMAKtra+s809u3QSNQYeWUopcRH5a7hlsi4vKGC1UV5QYhZarOTTdv120629WNWCuQrcFti5JR2dzacvWrgZ+p5vRRY9PeR+fbVmGTAbgkxA0RKt9AH87JeV7V+dbQ0I66trYOjJJgN1ExC/HLQD5nKs6xBceOHxiyrFOquaunzHwB8YmaKyRrERQAafruY8Veg35f7sQ4D71+8YvdGN3KZEXl9e/mnE2GvcRYe7D/NZWQuYbxDPRZqyMSEaFKgY1v5hUo84eQGTmw63JPfwLlxSbnC+I1HUGssrn1pMoqKCGzTwcCC1Mhw4kIXT2B+jxUScRnd0L7EyijI0BFCFeME73/y8HwfpUMU/K85aKsEglaMnQXYkLoO7VrtnJwYnJOGeckzzB+INcTTMZ7/AcPPwr12tbVExLJFVPQEZrnxblF/l+VlF2UbzQ577hvWQc9hDBKCCccVye/f+SdRPOrQuw45ziOki0Agc9B8ghjdrwgm7gqMUoEMUwWrcQmhu281NO/ZX5pl5fSkhiFCFkwweNZYP/NGPsEwF7EkhAXguuWhrwkEmKAWZ2IVzqdWoEouDhm9DfnvN/lWOKWqlwFspJyVEYVfZwgO710CZHzhhgLYxwJiWAuSADUSqZULCXqXCJTNcV0S3QJi8AiEbLCqVSNVa/rMlJKU41HCSa6vFenR/Sc2zgiIfKoUepSqFSgJJIQWpDwRu4u1HZVmAlFIr8xOafWSPvaON9aDyVFYh+vEeu9s41t84oLvJTEkR+22PmgGTlCAG4QwkzGr7oZT5WyRiNIoXmEZvQ8Xr1TVT06c6c/UNHcfBajeYXoNFNFTFRry3OxuurZqbm+N+XO3QMDr5Q0Hd+D+PxGTODifI24NFTvGlXhL799P6xMer41Me8ZSQFRkXQRM95kn/dpVaeOvrstsrzQ+6noOZWP0IW80XD3495bzSoh8jye9TtK5xZKQmcCMd6+fWVghZfSFXInk/Gep8+c6YS7vMYxoJK9vjyYBYDVnzv/74GI1SgLQkEmrp8196dIMQNUQM4TKABSlJ/3C1Xnvkj4AGKTNBURSuh2Ddka4rLIrlD/HtWNEzyen58OBMqQHgGAggAApLum+ocqawCAkRKASILoExzJ0C0N+6izCquyufVkhLEu1aDzCse/fyKwtFhQINliiexfCADaVVle7vd531LdEIyE3951uSeIxBmvqGMUThZhH+V0ODr4yf/01mu0KFw40b+/pWJZMdQkqKI8bdqP4ULxwpnj8/+smo9xHvrtPy+8iviH5rrE7xRQOTlMBsB68vTpjjvhsLJaZBBSVlrgP3WuomIxYr930IW7MglRMq6vWfXsnIK8UwQoVM118e5X9Tsv9QSheFhuSADcm6n8hDwYjS+M3trVH8rpsIghi+27MnB3+5KPmq8kECymDtK1cuWiGeNzXnV6ox4yIw0zDn3wPNR1T9fvOJJZr/ZTkgMto35OkX/bI6V/cyKDA8GwZe0bZPxE0eGjf9cJ1ri8rHBewZTyCR7jiUTfVNw3rSMPH2p8DuoCsKvdQlbSTT/RMmKIAGD8fnHp7HUziv7oRIYNDgQZ5+2E82AEaAcAz8hTd/s9xX3TOrL9Qlf9rss9fdCTYH+DlTEi7L660pmBYcso3Dq/5B2nd5WZwMhyqEfs1i6SYJcHXWe+qRDhRAYFQC/XVL30kM+3OYmxXYFxHuoeuL91wbHjdpHYqVKeVN3CSEIOx3xe/PuNS1dOFeWOa5g3fkKph9KZScyhRciMNGz7vHP9hn90nIH+nYhsAa6TvWSjPlV1WbaOmPN/XbKkdOlDhRsLPN51Sc4FxnkoZEYOvHfjxtvSe1Q5etQ5x6wRId4jkiHuJiIx0T4/+fr0gudmzVk+Lde3vMAzbrVcz7BhMt7TGwk3/OvuvVM1LZ82wyGOkY5iGA0kQYKoVLKQt1RxN5EtRW7k4qrKzVNzcl5UDXxzcHDnN45+uAN65Tmcs0sgSRKA1D9BFt8N2L/Fa07VazdVcTHZcyJClUylVARK51tseUJxz7YFtK2AY9RiQAnRp8PD12Szl32AruiSciUs3Y/SRcsARt9GyRYD4QiDEAYNRq7pHKGuwJJOKRBA5ipI9lhydikmTwYQF3uI99gKOb1BVymfNglAcnFEsnB6UrIS8nJwuyNkhAQgu0SIUAU5um3RKZXOOAE2svWPKypBZWcqv4+UiZF3hIz4Ah3GyiJU0BV+dARkjQRgbP6nS1ZAtAgVZF+QdRKAzO4abubRHW3onOH/DRGq+XRzp5Q9pouxJsLtvGNGwAM8wAM44r9xrwlJHa7ZngAAAABJRU5ErkJggg=='

	});
}

function watchCurrentPosition() {
	var positionTimer = navigator.geolocation.watchPosition(
			function (position) {
				setMarkerPosition(
						currentPositionMarker,
						position
						);
			});
}